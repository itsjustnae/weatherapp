import React from 'react';
import Weather from './Weather';

function App() {


  return (
    <div className="panel">
      <div className="panel-front">
        <Weather />
      </div>
    </div>
  );
}

export default App;
