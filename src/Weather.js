import React, { Component } from 'react';
import moment from 'moment';

export class Weather extends Component {

  render() {
    return (
      <div>
        <div className="date">{moment().format('dddd')}</div>
        <div className="time">{moment().format('LT')}</div>
      <div>
      <i className="far fa-sun fa-7x sun"></i>
      </div>
        <div className="temp">65 <i className='fas fa-temperature-high degree'></i></div>
        <hr className="hr" />
        <div>Hartford</div>
      </div>
    )
  }
}

export default Weather;
